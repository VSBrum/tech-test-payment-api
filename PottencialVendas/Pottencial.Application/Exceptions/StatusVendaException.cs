﻿using System;

namespace Pottencial.Application.Exceptions
{
    public class StatusVendaException : Exception
    {
        public StatusVendaException(string msgErro) : base(msgErro)
        {
        }
    }
}
