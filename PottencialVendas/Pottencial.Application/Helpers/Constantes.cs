﻿namespace Pottencial.Application.Helpers
{
    public class Constantes
    {
        public class StatusVenda
        {
            public const string StatusNaoPodeAtualizar = "O status atual não pode ser atualizado";
            public const string StatusInvalido = "Status inválido!";
            public const string StatusAtualizadoErrado = "Status atual não pode ser atualizado para o status informado, verifique a documentação para visualizar as possíves trocas de status.";
        }

        public class Mardown
        {
            public const string SummaryStatus = "Muda o status de uma venda já registrada.";
            public const string DescriptionStatus = @"Os status serão atualizados de acordo com a regra a seguir, qualquer formato fora deste padrão não será registrado:
                                                        De: ""Aguardando pagamento"" Para: ""Cancelada""
                                                        De: ""Pagamento Aprovado"" Para: ""Enviado para Transportadora""
                                                        De: ""Pagamento Aprovado"" Para: ""Cancelada""
                                                        De: ""Enviado para Transportador"" Para: ""Entregue""
                                                     Códigos de cada status:
                                                        Aguardando pagamento = 1
                                                        Pagamento Aprovado = 2
                                                        Enviado para Transportadora  = 3
                                                        Entregue = 4
                                                        Cancelada  = 5";
        }
    }
}
