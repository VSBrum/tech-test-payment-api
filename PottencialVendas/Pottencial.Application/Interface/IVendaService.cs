﻿using Pottencial.Application.ModelsDto;
using System.Threading.Tasks;
using static Pottencial.Domain.Enums.EnumVenda;

namespace Pottencial.Application.Interface
{
    public interface IVendaService
    {
        Task<VendaDetalhadaDto> ObterPorId(int id);

        Task Inserir(VendaDto venda);

        Task AlterarStatusVenda(int idVenda, StatusVenda status);
    }
}
