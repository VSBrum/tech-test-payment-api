﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Application.ModelsDto
{
    public class ProdutoDto
    {
        [Display(Description = "Descição")]
        [Required(ErrorMessage = "Descrição do item é obrigatória")]
        public string Descricao { get; set; }

        [Display(Description = "Quantidade")]
        [Range(1, int.MaxValue, ErrorMessage = "Quantidade de itens é obrigatória")]
        public ushort Quantidade { get; set; }

        [Display(Description = "Valor Produto")]
        [Required(ErrorMessage = "Valor do item é obrigatório")]
        public double ValorProduto { get; set; }
    }
}
