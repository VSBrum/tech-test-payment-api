﻿using System;
using System.Collections.Generic;
using static Pottencial.Domain.Enums.EnumVenda;

namespace Pottencial.Application.ModelsDto
{
    public class VendaDetalhadaDto
    {
        public int IdVenda { get; set; }
        public int IdVendedor { get; set; }
        public DateTime DataVenda { get; set; }
        public Guid IdentificadorVenda { get; set; }
        public StatusVenda StatusVenda { get; set; }
        public VendedorDto Vendedor { get; set; }
        public List<ProdutoDto> Produtos { get; set; }
    }
}
