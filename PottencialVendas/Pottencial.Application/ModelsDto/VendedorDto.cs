﻿using System.ComponentModel.DataAnnotations;

namespace Pottencial.Application.ModelsDto
{
    public class VendedorDto
    {
        [Required]
        public string Nome { get; set; }
        [Required]
        public string Cpf { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Telefone { get; set; }
    }
}
