﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Pottencial.Application.ModelsDto
{
    public class VendaDto
    {
        public VendedorDto Vendedor { get; set; }

        [Display(Description = "Produto")]
        [Required(ErrorMessage = "Nenhum item encontrado, é necessário informar ao menos 1 item.")]
        public List<ProdutoDto> Produtos { get; set; }
    }
}
