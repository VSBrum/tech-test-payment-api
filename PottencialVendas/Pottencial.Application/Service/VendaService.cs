﻿using AutoMapper;
using Pottencial.Application.Exceptions;
using Pottencial.Application.Helpers;
using Pottencial.Application.Interface;
using Pottencial.Application.ModelsDto;
using Pottencial.Data.Interface;
using Pottencial.Domain.Entities;
using Pottencial.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Pottencial.Domain.Enums.EnumVenda;

namespace Pottencial.Application.Service
{
    public class VendaService : IVendaService
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWork unitOfWork;
        private readonly IRepositorioVenda repositorioVenda;
        private readonly IRepositorioVendedor repositorioVendedor;

        public VendaService(IMapper mapper, IUnitOfWork unitOfWork, IRepositorioVenda repositorioVenda, IRepositorioVendedor repositorioVendedor)
        {
            _mapper = mapper;
            this.unitOfWork = unitOfWork;
            this.repositorioVenda = repositorioVenda;
            this.repositorioVendedor = repositorioVendedor;
        }

        public async Task<VendaDetalhadaDto> ObterPorId(int id)
        {
            var venda = repositorioVenda.ObterPorId(id);

            return _mapper.Map<VendaDetalhadaDto>(venda);
        }

        public async Task Inserir(VendaDto vendaDto)
        {
            ValidarVenda(vendaDto);

            var vendedor = _mapper.Map<Vendedor>(vendaDto.Vendedor);
            var produtos = _mapper.Map<List<Produto>>(vendaDto.Produtos);

            var venda = new Venda()
            {
                Vendedor = VendedorExistente(vendedor),
                Produtos = produtos
            };

            repositorioVenda.Adicionar(venda);
            unitOfWork.Commit();
        }

        public async Task AlterarStatusVenda(int idVenda, StatusVenda status)
        {
            var venda = repositorioVenda.ObterPorId(idVenda);

            if (venda is null)
                throw new StatusVendaException($"Venda com id [{idVenda}] não encontrada.");

            switch (venda.StatusVenda)
            {
                case EnumVenda.StatusVenda.Aguardando_pagamento:
                    if (!(status == EnumVenda.StatusVenda.Pagamento_aprovado || status == EnumVenda.StatusVenda.Cancelada))
                        throw new StatusVendaException(Constantes.StatusVenda.StatusAtualizadoErrado);
                    else
                        AlterarStatus(venda, status);
                    break;

                case EnumVenda.StatusVenda.Pagamento_aprovado:
                    if (!(status == EnumVenda.StatusVenda.Enviado_para_transportadora || status == EnumVenda.StatusVenda.Cancelada))
                        throw new StatusVendaException(Constantes.StatusVenda.StatusAtualizadoErrado);
                    else
                        AlterarStatus(venda, status);
                    break;

                case EnumVenda.StatusVenda.Enviado_para_transportadora:
                    if (status != EnumVenda.StatusVenda.Entregue)
                        throw new StatusVendaException(Constantes.StatusVenda.StatusAtualizadoErrado);
                    else
                        AlterarStatus(venda, status);
                    break;

                case EnumVenda.StatusVenda.Cancelada:
                    throw new StatusVendaException(Constantes.StatusVenda.StatusNaoPodeAtualizar);
                case EnumVenda.StatusVenda.Entregue:
                    throw new StatusVendaException(Constantes.StatusVenda.StatusNaoPodeAtualizar);
                default:
                    throw new StatusVendaException(Constantes.StatusVenda.StatusInvalido);
            }
        }

        private void AlterarStatus(Venda venda, StatusVenda status)
        {
            venda.StatusVenda = status;
            repositorioVenda.Alterar(venda);

            unitOfWork.Commit();
        }

        private Vendedor VendedorExistente(Vendedor vendedor)
        {
            var vendedorExistente = repositorioVendedor.ObterPorCpf(vendedor.Cpf);

            if (vendedorExistente is null)
                return vendedor;

            return vendedorExistente;
        }

        private void ValidarVenda(VendaDto venda)
        {
            if (venda.Vendedor != null)
            {
                var cpf = venda.Vendedor.Cpf;
                var email = venda.Vendedor.Email;
                var nome = venda.Vendedor.Nome;

                if (!Validacoes.ValidarCPF(cpf))
                    throw new Exception($"Cpf [{cpf}] do vendedor [{nome}] é inválido.");

                if (!Validacoes.ValidarEmail(email))
                    throw new Exception($"Email [{email}] do vendedor [{nome}] é inválido.");
            }
            else
                throw new Exception("Nenhum vendedor encontrado, adicione um vendedor a venda.");

            if (venda.Produtos == null || !venda.Produtos.Any())
                throw new Exception("Nenhum item encontrado, é necessário informar ao menos 1 item.");
        }
    }
}
