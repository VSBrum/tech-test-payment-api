﻿using AutoMapper;
using Pottencial.Application.ModelsDto;
using Pottencial.Domain.Entities;

namespace Pottencial.Application.AutoMapper
{
    public class AutoMapperSetup : Profile
    {
        public AutoMapperSetup()
        {
            CreateMap<VendaDto, Venda>().ReverseMap();
            CreateMap<VendaDetalhadaDto, Venda>().ReverseMap();
            CreateMap<ProdutoDto, Produto>().ReverseMap();
            CreateMap<VendedorDto, Vendedor>().ReverseMap();
        }
    }
}
