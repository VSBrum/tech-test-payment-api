﻿using Microsoft.EntityFrameworkCore;
using Pottencial.Data.Context;
using Pottencial.Data.Interface;
using Pottencial.Domain.Entities;
using System.Linq;

namespace Pottencial.Data.Repositories
{
    public class RepositorioVenda : RepositorioBase<Venda>, IRepositorioVenda
    {
        private readonly DataContext dataContext;

        public RepositorioVenda(DataContext dataContext)
           : base(dataContext)
        {
            this.dataContext = dataContext;
        }

        public Venda ObterPorId(int id)
        {
            return dataContext.Venda
                              .Include(a => a.Vendedor)
                              .Include(a => a.Produtos)
                              .Where(a => a.IdVenda == id).FirstOrDefault();
        }
    }
}
