﻿using Pottencial.Data.Context;
using Pottencial.Data.Interface;
using Pottencial.Domain.Entities;
using System.Linq;

namespace Pottencial.Data.Repositories
{
    public class RepositorioVendedor : RepositorioBase<Vendedor>, IRepositorioVendedor
    {
        private readonly DataContext dataContext;
        public RepositorioVendedor(DataContext dataContext)
           : base(dataContext)
        {
            this.dataContext = dataContext;
        }

        public Vendedor ObterPorCpf(string cpf)
        {
            return dataContext.Vendedor.Where(v => v.Cpf == cpf).FirstOrDefault();
        }
    }
}
