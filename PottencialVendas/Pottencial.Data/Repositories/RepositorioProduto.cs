﻿using Pottencial.Data.Context;
using Pottencial.Data.Interface;
using Pottencial.Domain.Entities;

namespace Pottencial.Data.Repositories
{
    public class RepositorioProduto : RepositorioBase<Produto>, IRepositorioProduto
    { 
        public RepositorioProduto(DataContext dataContext)
           : base(dataContext)
        {
        }
    }
}
