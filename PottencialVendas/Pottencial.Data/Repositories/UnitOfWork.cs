﻿using Pottencial.Data.Context;
using Pottencial.Data.Interface;

namespace Pottencial.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext dataContext;

        public UnitOfWork(DataContext dataContext)
        {
            this.dataContext = dataContext;
        }

        public void Commit()
        {
            dataContext.SaveChanges();
        }
    }
}
