﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Pottencial.Data.Interface
{
    public interface IRepositorioBase<TEntity> : IDisposable where TEntity : class
    {
        TEntity ObterPorId(Expression<Func<TEntity, bool>> filtro);
        List<TEntity> ObterTodos();
        void Adicionar(TEntity entity);
        void Alterar(TEntity entity);
        void Excluir(TEntity entity);
    }
}
