﻿using Pottencial.Domain.Entities;

namespace Pottencial.Data.Interface
{
    public interface IRepositorioVenda : IRepositorioBase<Venda>
    {
        Venda ObterPorId(int id);
    }
}
