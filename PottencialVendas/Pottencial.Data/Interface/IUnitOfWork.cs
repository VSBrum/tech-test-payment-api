﻿namespace Pottencial.Data.Interface
{
    public interface IUnitOfWork
    {
        void Commit();
    }
}
