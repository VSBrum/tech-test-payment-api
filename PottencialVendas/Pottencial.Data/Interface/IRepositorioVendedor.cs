﻿using Pottencial.Domain.Entities;

namespace Pottencial.Data.Interface
{
    public interface IRepositorioVendedor : IRepositorioBase<Vendedor>
    {
        Vendedor ObterPorCpf(string cpf);
    }
}
