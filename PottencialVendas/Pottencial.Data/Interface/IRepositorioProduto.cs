﻿using Pottencial.Domain.Entities;

namespace Pottencial.Data.Interface
{
    public interface IRepositorioProduto : IRepositorioBase<Produto>
    {
    }
}
