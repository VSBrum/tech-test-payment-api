﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Pottencial.Data.Mapping
{
    public class VendedorMap : IEntityTypeConfiguration<Vendedor>
    {
        public void Configure(EntityTypeBuilder<Vendedor> builder)
        {
            builder.ToTable("Vendedor");

            builder.HasIndex(i => i.Cpf, "uidx_cpf")
                .IsUnique();

            builder.HasKey(v => v.IdVendedor);

            builder.Property(v => v.IdVendedor)
                .ValueGeneratedOnAdd();

            builder.Property(v => v.Nome)
                .IsRequired()
                .HasMaxLength(120)
                .HasColumnName("Nome");

            builder.Property(v => v.Cpf)
                .IsRequired()
                .HasMaxLength(11);

            builder.Property(v => v.Email)
                .IsRequired()
                .HasMaxLength(80);

            builder.Property(v => v.Telefone)
                .IsRequired()
                .HasMaxLength(15)
                .HasColumnName("Telefone");
        }
    }
}
