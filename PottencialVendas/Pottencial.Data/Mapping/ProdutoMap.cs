﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Domain.Entities;

namespace Pottencial.Data.Mapping
{
    public class ProdutoMap : IEntityTypeConfiguration<Produto>
    {
        public void Configure(EntityTypeBuilder<Produto> builder)
        {
            builder.ToTable("ProdutoVenda");

            builder.HasKey(v => v.IdProduto);

            builder.Property(v => v.Descricao)
                .IsRequired()
                .HasMaxLength(200)
                .HasColumnName("Descricao");

            builder.Property(v => v.Quantidade)
                .IsRequired()
                .HasColumnName("Quantidade");

            builder.Property(v => v.ValorProduto)
                .IsRequired()
                .HasColumnName("ValorProduto");

            builder.HasOne<Venda>()
                .WithMany(v => v.Produtos)
                .HasForeignKey(v => v.IdVenda);
        }
    }
}
