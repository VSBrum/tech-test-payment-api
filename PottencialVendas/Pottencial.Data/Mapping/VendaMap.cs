﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pottencial.Domain.Entities;

namespace Pottencial.Data.Mapping
{
    public class VendaMap : IEntityTypeConfiguration<Venda>
    {
        public void Configure(EntityTypeBuilder<Venda> builder)
        {
            builder.ToTable("Vendas");

            builder.HasKey(v => v.IdVenda);

            builder.Property(v => v.IdVendedor)
                .IsRequired()
                .HasColumnName("IdVendedor");

            builder.Property(v => v.IdentificadorVenda)
                .IsRequired()
                .HasMaxLength(16)
                .HasColumnName("IdentificadorVenda");

            builder.Property(v => v.StatusVenda)
                .IsRequired()
                .HasColumnName("Status");

            builder.Property(v => v.DataVenda)
                .IsRequired()
                .HasColumnName("DataVenda");

            builder.HasOne<Vendedor>(m => m.Vendedor)
                .WithMany(g => g.Vendas)
                .HasForeignKey(m => m.IdVendedor);
        }
    }
}
