﻿using Pottencial.Application.ModelsDto;
using Pottencial.Domain.Entities;
using System;
using System.Collections.Generic;
using static Pottencial.Domain.Enums.EnumVenda;

namespace Pottencial.Test.Base
{
    public class BaseRespositorioTest
    {
        protected Venda VendaCorreta()
        {
            return new Venda()
            {
                DataVenda = DateTime.Now,
                IdentificadorVenda = Guid.NewGuid(),
                Produtos = new List<Produto>()
                {
                    new Produto() { Descricao = "Teste 1", Quantidade = 1 },
                    new Produto() { Descricao = "Teste 2", Quantidade = 2 },
                    new Produto() { Descricao = "Teste 3", Quantidade = 3 }
                },
                StatusVenda = StatusVenda.Aguardando_pagamento,
                Vendedor = new Vendedor()
                {
                    Cpf = "74614138055",
                    Nome = "Vinicius ",
                    Email = "vinicius@gmail.com",
                    Telefone = "999999-9999",
                }
            };
        }

        protected VendaDto VendaDtoCorreta()
        {
            return new VendaDto()
            {
                Produtos = new List<ProdutoDto>()
                {
                    new ProdutoDto() { Descricao = "Teste 1", Quantidade = 1 },
                    new ProdutoDto() { Descricao = "Teste 2", Quantidade = 2 },
                    new ProdutoDto() { Descricao = "Teste 3", Quantidade = 3 }
                },
                Vendedor = new VendedorDto()
                {
                    Cpf = "17698785050",
                    Nome = "Vinicius ",
                    Email = "vinicius@gmail.com",
                    Telefone = "999999-9999",
                }
            };
        }

        protected VendaDto VendaDtoSemProduto()
        {
            return new VendaDto()
            {
                Produtos = null,
                Vendedor = new VendedorDto()
                {
                    Cpf = "37125560040",
                    Nome = "Vinicius ",
                    Email = "vinicius@gmail.com",
                    Telefone = "999999-9999",
                }
            };
        }

        protected VendaDto VendaDtoSemVendedor()
        {
            return new VendaDto()
            {
                Produtos = new List<ProdutoDto>()
                {
                    new ProdutoDto() { Descricao = "Teste 1", Quantidade = 1 },
                    new ProdutoDto() { Descricao = "Teste 2", Quantidade = 2 },
                    new ProdutoDto() { Descricao = "Teste 3", Quantidade = 3 }
                },
                Vendedor = null
            };
        }

        protected VendaDto VendaDtoCpfInvalido()
        {
            return new VendaDto()
            {
                Produtos = new List<ProdutoDto>()
                {
                    new ProdutoDto() { Descricao = "Teste 1", Quantidade = 1 },
                    new ProdutoDto() { Descricao = "Teste 2", Quantidade = 2 },
                    new ProdutoDto() { Descricao = "Teste 3", Quantidade = 3 }
                },
                Vendedor = new VendedorDto()
                {
                    Cpf = "12345687956",
                    Nome = "Vinicius ",
                    Email = "vinicius@gmail.com",
                    Telefone = "999999-9999",
                }
            };
        }

        protected VendaDto VendaDtoEmailInvalido()
        {
            return new VendaDto()
            {
                Produtos = new List<ProdutoDto>()
                {
                    new ProdutoDto() { Descricao = "Teste 1", Quantidade = 1 },
                    new ProdutoDto() { Descricao = "Teste 2", Quantidade = 2 },
                    new ProdutoDto() { Descricao = "Teste 3", Quantidade = 3 }
                },
                Vendedor = new VendedorDto()
                {
                    Cpf = "37125560040",
                    Nome = "Vinicius ",
                    Email = "viniciusgmail.com",
                    Telefone = "999999-9999",
                }
            };
        }
    }
}
