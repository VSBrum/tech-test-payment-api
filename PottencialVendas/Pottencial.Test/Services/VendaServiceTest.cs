﻿using AutoMapper;
using Moq;
using Pottencial.Application.AutoMapper;
using Pottencial.Application.Helpers;
using Pottencial.Application.Service;
using Pottencial.Data.Interface;
using Pottencial.Test.Base;
using Xunit;
using static Pottencial.Domain.Enums.EnumVenda;

namespace Pottencial.Test.Services
{
    public class VendaServiceTest : BaseRespositorioTest
    {
        private Mock<IRepositorioVenda> repositorioVendaMock = new Mock<IRepositorioVenda>();
        private Mock<IRepositorioVendedor> repositorioVendedorMock = new Mock<IRepositorioVendedor>();
        private Mock<IUnitOfWork> unitOfWorkMock = new Mock<IUnitOfWork>();

        [Fact]
        private void AlterarStatusCorretamente()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperSetup());
            });

            var mapper = config.CreateMapper();

            repositorioVendaMock.Setup(x => x.ObterPorId(1)).Returns(VendaCorreta());

            var vendaService = new VendaService(mapper, unitOfWorkMock.Object, repositorioVendaMock.Object, repositorioVendedorMock.Object);

            var result = vendaService.AlterarStatusVenda(1, StatusVenda.Cancelada);

            Assert.True(result.Exception == null);
        }

        [Fact]
        private void AlterarStatusIncorretamente()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperSetup());
            });

            var mapper = config.CreateMapper();

            repositorioVendaMock.Setup(x => x.ObterPorId(1)).Returns(VendaCorreta());

            var vendaService = new VendaService(mapper, unitOfWorkMock.Object, repositorioVendaMock.Object, repositorioVendedorMock.Object);

            var result = vendaService.AlterarStatusVenda(1, StatusVenda.Enviado_para_transportadora);
            var msg = result.Exception.InnerException.Message;

            Assert.True(msg.Equals(Constantes.StatusVenda.StatusAtualizadoErrado));
        }

        [Fact]
        private void InseriVendaCorreta()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperSetup());
            });

            var mapper = config.CreateMapper();

            repositorioVendaMock.Setup(x => x.Adicionar(VendaCorreta()));

            var vendaService = new VendaService(mapper, unitOfWorkMock.Object, repositorioVendaMock.Object, repositorioVendedorMock.Object);

            var result = vendaService.Inserir(VendaDtoCorreta());

            Assert.True(result.Exception == null);
        }

        [Fact]
        private void InseriVendaIncorretaSemProduto()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperSetup());
            });

            var mapper = config.CreateMapper();

            repositorioVendaMock.Setup(x => x.Adicionar(VendaCorreta()));

            var vendaService = new VendaService(mapper, unitOfWorkMock.Object, repositorioVendaMock.Object, repositorioVendedorMock.Object);

            var result = vendaService.Inserir(VendaDtoSemProduto());

            var msg = result.Exception.InnerException.Message;

            Assert.True(msg.Equals("Nenhum item encontrado, é necessário informar ao menos 1 item."));
        }

        [Fact]
        private void InseriVendaIncorretaSemVendedor()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperSetup());
            });

            var mapper = config.CreateMapper();

            repositorioVendaMock.Setup(x => x.Adicionar(VendaCorreta()));

            var vendaService = new VendaService(mapper, unitOfWorkMock.Object, repositorioVendaMock.Object, repositorioVendedorMock.Object);

            var result = vendaService.Inserir(VendaDtoSemVendedor());

            var msg = result.Exception.InnerException.Message;

            Assert.True(msg.Equals("Nenhum vendedor encontrado, adicione um vendedor a venda."));
        }

        [Fact]
        private void InseriVendaIncorretaCpfInvalido()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperSetup());
            });

            var mapper = config.CreateMapper();

            repositorioVendaMock.Setup(x => x.Adicionar(VendaCorreta()));

            var vendaService = new VendaService(mapper, unitOfWorkMock.Object, repositorioVendaMock.Object, repositorioVendedorMock.Object);

            var result = vendaService.Inserir(VendaDtoCpfInvalido());

            var cpf = VendaDtoCpfInvalido().Vendedor.Cpf;
            var nome = VendaDtoCpfInvalido().Vendedor.Nome;

            var msg = result.Exception.InnerException.Message;

            Assert.True(msg.Equals($"Cpf [{cpf}] do vendedor [{nome}] é inválido."));
        }

        [Fact]
        private void InseriVendaIncorretaEmailInvalido()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new AutoMapperSetup());
            });

            var mapper = config.CreateMapper();

            repositorioVendaMock.Setup(x => x.Adicionar(VendaCorreta()));

            var vendaService = new VendaService(mapper, unitOfWorkMock.Object, repositorioVendaMock.Object, repositorioVendedorMock.Object);

            var result = vendaService.Inserir(VendaDtoEmailInvalido());

            var email = VendaDtoEmailInvalido().Vendedor.Email;
            var nome = VendaDtoEmailInvalido().Vendedor.Nome;

            var msg = result.Exception.InnerException.Message;

            Assert.True(msg.Equals($"Email [{email}] do vendedor [{nome}] é inválido."));
        }
    }
}

