﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Pottencial.Application.Helpers;
using Pottencial.Application.Interface;
using Pottencial.Application.ModelsDto;
using Swashbuckle.AspNetCore.Annotations;
using static Pottencial.Domain.Enums.EnumVenda;

namespace Pottencial.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendaController : ControllerBase
    {
        private readonly IVendaService vendaService;

        public VendaController(IVendaService vendaService)
        {
            this.vendaService = vendaService;
        }

        [HttpGet]
        [Route("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> ObterPorId(int id)
        {
            if (!(id > 0)) return BadRequest("Id da venda é obrigatório");

            var venda = await vendaService.ObterPorId(id);

            if (venda is null)
                return NotFound();

            return Ok(venda);
        }

        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> RealizarVenda(VendaDto venda)
        {
            if (!ModelState.IsValid) return BadRequest();

            await vendaService.Inserir(venda);
            return Ok();
        }

        [HttpPut]
        [Route("{id}")]
        [SwaggerOperation(Constantes.Mardown.SummaryStatus, Constantes.Mardown.DescriptionStatus)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> AlterarStatus(int id, StatusVenda novoStatus)
        {
            if (!(id > 0)) return BadRequest("Id da venda é obrigatório");

            var venda = vendaService.ObterPorId(id);

            if (venda is null)
                return NotFound();

            await vendaService.AlterarStatusVenda(id, novoStatus);

            return Ok("Status atualizado com sucesso!");
        }
    }
}
