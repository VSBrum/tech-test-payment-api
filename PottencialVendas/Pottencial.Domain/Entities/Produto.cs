﻿namespace Pottencial.Domain.Entities
{
    public class Produto
    {
        public int IdProduto { get; set; }
        public string Descricao { get; set; }
        public ushort Quantidade { get; set; }
        public double ValorProduto { get; set; }
        public int IdVenda { get; set; }
    }
}
