﻿using System;
using System.Collections.Generic;
using static Pottencial.Domain.Enums.EnumVenda;

namespace Pottencial.Domain.Entities
{
    public class Venda
    {
        public int IdVenda { get; set; }
        public int IdVendedor { get; set; }
        public DateTime DataVenda { get; set; }
        public Guid IdentificadorVenda { get; set; }
        public StatusVenda StatusVenda { get; set; }
        public Vendedor Vendedor { get; set; }
        public List<Produto> Produtos { get; set; }

        public Venda()
        {
            IdentificadorVenda = Guid.NewGuid();
            StatusVenda = StatusVenda.Aguardando_pagamento;
            DataVenda = DateTime.Now;
        }
    }
}
