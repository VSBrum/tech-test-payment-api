﻿using System.Collections.Generic;

namespace Pottencial.Domain.Entities
{
    public class Vendedor
    {
        public int IdVendedor { get; set; }
        public string Nome { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }

        public List<Venda> Vendas { get; set; }
    }
}
