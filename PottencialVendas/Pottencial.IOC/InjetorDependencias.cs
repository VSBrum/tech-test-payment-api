﻿using Microsoft.Extensions.DependencyInjection;
using Pottencial.Application.Interface;
using Pottencial.Application.Service;
using Pottencial.Data.Context;
using Pottencial.Data.Interface;
using Pottencial.Data.Repositories;

namespace Pottencial.IOC
{
    public class InjetorDependencias
    {
        public static void InjetarDependencias(IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IVendaService, VendaService>();
            services.AddScoped<IRepositorioVenda, RepositorioVenda>();
            services.AddScoped<IRepositorioVendedor, RepositorioVendedor>();
            services.AddScoped<IRepositorioProduto, RepositorioProduto>();
            services.AddScoped<DataContext, DataContext>();
        }
    }
}
